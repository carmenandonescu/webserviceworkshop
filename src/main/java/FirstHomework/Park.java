package FirstHomework;

public class Park extends Attraction {

    public Park(String name, String address) {
        super(name, address);
    }

    @Override
    public boolean isOpen() {
        return true;
    }

}
