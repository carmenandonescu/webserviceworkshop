package FirstHomework;

import java.time.LocalTime;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        /*
        setup city
         */
        City london;
        Museum britishMuseum = new Museum("British Museum", "Great Russell Street;",
                LocalTime.of(9, 30), LocalTime.of(20, 0));
        Museum tateMuseum = new Museum("Tate Modern", "Bankside, London SE1 9TG",
                LocalTime.of(10, 0), LocalTime.of(18, 0));
        Cinema odeonCinema = new Cinema("Odeon Cinema", " Charlie Chaplin Walk",
                LocalTime.of(12, 0), LocalTime.of(23, 0));
        Cinema vueLondonCinema = new Cinema("Vue Cinema London - Piccadilly", " 19 Lower, Regent St, London SW1Y 4LR",
                LocalTime.of(12, 0), LocalTime.of(23, 0));
        Park hydePark = new Park("Hyde Park", "London");
        Park jamesPark = new Park("St. James's Park", "London SW1A 2BJ");

        london = new City("London", 9000,
                List.of(britishMuseum, tateMuseum, odeonCinema, vueLondonCinema, hydePark, jamesPark));

        /*
        adding movies
         */
        odeonCinema.addMovie((LocalTime.of(18, 30)), "X-men");
        odeonCinema.addMovie((LocalTime.of(19, 30)), "X-men: The Last Stand");
        odeonCinema.addMovie((LocalTime.of(20, 30)), "X-men: Apocalypse");
        vueLondonCinema.addMovie((LocalTime.of(18, 30)), "Batman");
        vueLondonCinema.addMovie((LocalTime.of(19, 30)), "Batman returns");
        vueLondonCinema.addMovie((LocalTime.of(20, 30)), "Batman - Dark knight");
        vueLondonCinema.addMovie((LocalTime.of(21, 30)), "Batman begins");

        PrintHelper helper = new PrintHelper(new StringBuffer());

        helper.printCinemaWithMostShows(london.getCinemaWithMostShows(), london.getNoOfShows());

        helper.printOpenedAttractions(london.getOpenAttractionsName());

        helper.printClosedAttractions(london.getClosedAttractionsName());

        helper.printGetHoursTillOpen(london.getHoursTillOpen());


    }
}
