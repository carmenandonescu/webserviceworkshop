package FirstHomework;

import java.time.LocalTime;
import java.util.*;

public class Cinema extends Attraction {

    Map<LocalTime, String> movies = new HashMap<>();

    public Cinema(String name, String address, LocalTime openingTime, LocalTime closingTime) {
        super(name, address, openingTime, closingTime);
    }

    public void addMovie(LocalTime time, String movie) {
        movies.put(time, movie);
    }

    public Map<LocalTime, String> getMovies() {
        return movies;
    }

    public int countMovies() {
        return getMovies().size();
    }

    @Override
    public boolean isOpen() {
        return getCurrentTime().isBefore(getClosingTime()) && getCurrentTime().isAfter(getOpeningTime());
    }
}
