package FirstHomework;

public interface Closable {

    boolean isOpen();

    default boolean isClosed(){
        return !isOpen();
    }
}
