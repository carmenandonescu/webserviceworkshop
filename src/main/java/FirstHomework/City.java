package FirstHomework;

import java.util.*;
import java.util.stream.Collectors;

public class City {
    private String name;
    private Integer population;
    private List<Attraction> listOfAttraction;

    public City(String name, Integer population, List<Attraction> attractions) {
        this.name = name;
        this.population = population;
        this.listOfAttraction = attractions;
    }

    public List<Attraction> getAttractions() {
        return listOfAttraction;
    }

    public List<String> getOpenAttractionsName() {
        return getAttractions().stream()
                .filter(Closable::isOpen)
                .map(Attraction::getName)
                .collect(Collectors.toList());
    }

    public List<String> getClosedAttractionsName() {
        return getClosedAttractions().stream()
                .map(Attraction::getName)
                .collect(Collectors.toList());
    }

    public List<Attraction> getClosedAttractions() {
        return getAttractions().stream()
                .filter(Closable::isClosed)
                .collect(Collectors.toList());
    }

    public Map<String, Map<Long, Long>> getHoursTillOpen() {
        return getClosedAttractions().stream()
                .collect(Collectors
                        .toMap(Attraction::getName, Attraction::getHoursAndMinutesUntilOpen));
    }

    public List<Cinema> getListOfCinemas() {

        return listOfAttraction.stream()
                .filter(attraction -> attraction instanceof Cinema)
                .map(attraction -> (Cinema) attraction)
                .collect(Collectors.toList());
    }

    public int getNoOfShows() {
        return getCinemaWithMostShows().countMovies();
    }

    public Cinema getCinemaWithMostShows() {
        return getListOfCinemas().stream()
                .max(Comparator.comparing(Cinema::countMovies))
                .orElseThrow(NoSuchElementException::new);
    }

    public void addAttraction(Attraction attraction) {
        listOfAttraction.add(attraction);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }
}
