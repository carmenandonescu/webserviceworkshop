package FirstHomework;

import java.time.Duration;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public abstract class Attraction implements Closable {

    private String name;
    private String address;
    private LocalTime openingTime;
    private LocalTime closingTime;
//    private LocalTime currentTime = LocalTime.now();
    private LocalTime currentTime = LocalTime.of(7,35);

    public Attraction(String name, String address, LocalTime openingTime, LocalTime closingTime) {
        this.name = name;
        this.address = address;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
    }

    public Attraction(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Map<Long,Long> getHoursAndMinutesUntilOpen(){
        long diffminutes = Duration.between(currentTime, openingTime).toMinutes();
        Map <Long,Long> map = new HashMap<>();
        long hours = diffminutes /60;
        long minutes = diffminutes % 60;
        map.put(hours, minutes);
       return map;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalTime getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(LocalTime openingTime) {
        this.openingTime = openingTime;
    }

    public LocalTime getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(LocalTime closingTime) {
        this.closingTime = closingTime;
    }

    public LocalTime getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(LocalTime currentTime) {
        this.currentTime = currentTime;
    }

}
