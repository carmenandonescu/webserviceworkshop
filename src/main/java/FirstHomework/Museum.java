package FirstHomework;

import java.time.LocalTime;

public class Museum extends Attraction {

    public Museum(String name, String address, LocalTime openingTime, LocalTime closingTime) {
        super(name, address, openingTime, closingTime);
    }

    @Override
    public boolean isOpen() {
        return getCurrentTime().isBefore(getClosingTime()) && getCurrentTime().isAfter(getOpeningTime());
    }

}


