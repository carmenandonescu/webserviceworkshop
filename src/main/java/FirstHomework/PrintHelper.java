package FirstHomework;

import java.util.List;
import java.util.Map;

public class PrintHelper {

    private StringBuffer message;

    public PrintHelper(StringBuffer message) {
        this.message = message;
    }

    public void printCinemaWithMostShows(Cinema cinema, int noOfShows) {
        message = new StringBuffer(" ");
        message.append("The cinema with the most shows is ")
                .append(cinema.getName())
                .append(" which has ")
                .append(noOfShows)
                .append(" shows");
        System.out.println(message + "\n");
    }

    public void printOpenedAttractions(List<String> listOfOpenedAttractions) {
        message = new StringBuffer(" ");
        if (listOfOpenedAttractions.size() > 0) {
            message.append("The following attractions are open: ")
                    .append(listOfOpenedAttractions);
        } else {
            message.append("There are no open attractions");
        }
        System.out.println(message + "\n");
    }

    public void printClosedAttractions(List<String> listOfClosedAttractions) {
        message = new StringBuffer(" ");
        if (listOfClosedAttractions.size() > 0) {
            message.append("The following attractions are closed: ")
                    .append(listOfClosedAttractions);
        } else {
            message.append("There are no closed attractions");
        }
        System.out.println(message + "\n");
    }

    public void printGetHoursTillOpen(Map<String, Map<Long, Long>> map) {
        message = new StringBuffer(" For the following attractions, the remaining time until open is:\n");
        map.forEach((k, v) -> {
                    message.append(" for " + k + " ");
                    v.forEach((key, value) -> message.append(key + " hours and  " + value + " minutes.\n"));
                });
        System.out.println(message + "\n");

    }
}
