package SecondHomework;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeManager {

    List<Employee> employeeList;

    public EmployeeManager(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public List<Employee> getEmployeesByDiscipline(Discipline disc) {
        return employeeList.stream()
                .filter(employee -> employee.getDiscipline().getName().equals(disc.getName()) &&
                        employee.getDiscipline().getLocation().equals(disc.getLocation()))
                .collect(Collectors.toList());
    }

    public Map<Integer, Employee> getEmployeesByDisciplineMap(Discipline disc) {
        return employeeList.stream()
                .filter(employee -> employee.getDiscipline().getName().equals(disc.getName()) &&
                        employee.getDiscipline().getLocation().equals(disc.getLocation()))
                .collect(Collectors.toMap(emp -> emp.getId(), emp -> emp));
    }
}
