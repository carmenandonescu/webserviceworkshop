package SecondHomework;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static final Logger LOGGER = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        LOGGER.info("Adding all employees");
        ArrayList<Employee> allEmployees = new ArrayList<>();
        allEmployees.add(new Employee(1, "Alexandra Alexandrescu", new Discipline("Testing", "ISD")));
        allEmployees.add(new Employee(2, "Bogdan Bogdanescu", new Discipline("Development", "ISD")));
        allEmployees.add(new Employee(3, "Constantin Constantinescu", new Discipline("Testing", "CLD")));
        allEmployees.add(new Employee(4, "Dumitru Dumitrescu", new Discipline("Testing", "ISD")));
        allEmployees.add(new Employee(5, "Florina Florescu", new Discipline("Development", "ISD")));


        EmployeeManager employeeManager = new EmployeeManager(allEmployees);
        Discipline testingIsd = new Discipline("Testing", "ISD");

        LOGGER.info("STANDARD WAY");
        employeeManager.getEmployeesByDiscipline(testingIsd)
                .forEach(employee -> LOGGER.debug(employee.toString()));

        LOGGER.info("MAP WAY");
        employeeManager.getEmployeesByDisciplineMap(testingIsd)
                .forEach((integer, employee) -> LOGGER.debug(integer + " : " + employee));


        LOGGER.info(" REGEX EXERCISE \n");
        RegexExercise regex = new RegexExercise();
        String firstPhrase = "Superman has 3 suits";
        String secondPhrase = "Batman has 5 cars";
        String thirdPhrase = "Dogs have no extra lives.";//no matching for count
        String fourthPhrase = "My name is "; //no matching for all categories
        String fifthPhrase = "";
        List<String> listOfPhrasesToTest = List.of(firstPhrase, secondPhrase, thirdPhrase, fourthPhrase, fifthPhrase);


        listOfPhrasesToTest.forEach(phrase -> {
            LOGGER.info("Phrase :" + phrase);
            LOGGER.info("Getting name : " + regex.extractValue(phrase, RegexExercise.Options.NAME));
            LOGGER.info("Getting no of items : " + regex.extractValue(phrase, RegexExercise.Options.COUNT));
            LOGGER.info("Getting item : " + regex.extractValue(phrase, RegexExercise.Options.ITEM));
            LOGGER.info("-------------------------------");

        });

    }

}
