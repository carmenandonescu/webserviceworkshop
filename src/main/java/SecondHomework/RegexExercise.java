package SecondHomework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExercise {

    public enum Options {
        NAME, COUNT, ITEM
    }

    public String extractValue(String phrase, Options option) {
        Matcher matcher;
        Pattern pattern = Pattern.compile(getPattern(option));

        try {
             matcher = pattern.matcher(phrase);
             if (matcher.find()){
                 return matcher.group(0);
             } else {
            throw new InvalidInputException ("No matching data in the given phrase");}

        } catch ( InvalidInputException exception ) {
            return exception.getMessage();
        }
    }

    private String getPattern(Options option) {
        switch (option) {
            case NAME:
                return "^[a-zA-Z]*man";
            case COUNT:
                return "[0-9]+";
            case ITEM:
                return "(\\w+)$";
            default:
                return "";

        }
    }

}
