package SecondHomework;

public class Employee {
    private int id;
    private String name;
    private Discipline discipline;

    public Employee(int id, String name, Discipline discipline) {
        this.id = id;
        this.name = name;
        this.discipline = discipline;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Discipline getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", discipline=" + discipline +
                '}';
    }
}
